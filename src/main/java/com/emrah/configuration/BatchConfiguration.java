package com.emrah.configuration;

import com.emrah.tasklet.ExploringTasklet;
import lombok.RequiredArgsConstructor;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BatchConfiguration {

	private final JobExplorer jobExplorer;
	private final JobBuilderFactory jobBuilderFactory;
	private final StepBuilderFactory stepBuilderFactory;

	@Bean
	public Tasklet explorerTasklet() {
		return new ExploringTasklet(jobExplorer);
	}
	
	@Bean
	public Step explorerStep() {
		return stepBuilderFactory.get("explorerStep")
				.tasklet(explorerTasklet())
				.build();
	}
	
	@Bean
	public Job job() {
		return jobBuilderFactory.get("job28")
				.start(explorerStep())
				.build();
	}
	
}
